#include <boost/scoped_ptr.hpp>
#include <boost/numeric/ublas/matrix.hpp>

class Strategy {
public:
    Strategy();
    ~Strategy();

private:
    boost::numeric::ublas::matrix<float> m1;
    boost::numeric::ublas::matrix<float> m2;
};

class Warrior {
public:
    Warrior();
    ~Warrior();

private:
    boost::scoped_ptr<Strategy> strat;
    int hp;
    int kills;
    int post_hp;
};

Warrior::Warrior() {
    hp = 100;
    kills = 0;
    post_hp = 100;
    strat.reset(new Strategy());
}
